<?php

use Illuminate\Database\Seeder;
use App\Models\Book;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Book::insert([
          [
            'ISBN' => '9780679783268',
            'title' => 'Pride and Prejudice',
            'description' => '',
            'publication_year' => '1813',
            'copies_available' => 500,
            'language' => 1
          ],
          [
            'ISBN' => '9780141441146',
            'title' => 'Jane Eyre',
            'description' => 'Jane Eyre is a novel by English writer Charlotte Brontë,
                              published under the pen name "Currer Bell", on 16 October 1847, by Smith, Elder & Co. of London.
                              The first American edition was published the following year by Harper & Brothers of New York',
            'publication_year' => '1847',
            'copies_available' => 50,
            'language' => 1
          ],
          [
            'ISBN' => '9781536640687',
            'title' => 'Utopia',
            'description' => '',
            'publication_year' => '2008',
            'copies_available' => 130,
            'language' => 2
          ],
          [
            'ISBN' => '9780375842207',
            'title' => 'The Book Thief',
            'description' => 'The Book Thief is a historical novel by Australian author Markus Zusak and is his most popular work.
                              Published in 2005, The Book Thief became an international bestseller and was translated into several languages.
                              It was adapted into a 2013 feature film of the same name.',
            'publication_year' => '2005',
            'copies_available' => 584,
            'language' => 1
          ],
          [
            'ISBN' => '9780553535648',
            'title' => 'Tell Me Three Things',
            'description' => '',
            'publication_year' => '2016',
            'copies_available' => 30,
            'language' => 1
          ],
        ]);
    }
}
