<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Category::insert([
        ['category' => 'Western'],
        ['category' => 'Teen'],
        ['category' => 'Travel'],
        ['category' => 'Crime'],
        ['category' => 'Sci-Fi & Fantasy'],
        ['category' => 'Science & Math'],
        ['category' => 'Romance'],
        ['category' => 'Drama'],
        ['category' => 'Medical'],
        ['category' => 'Mystery'],
        ['category' => 'Parenting'],
        ['category' => 'Literature & Fiction'],
        ['category' => 'Entertainment'],
        ['category' => 'Horror'],
        ['category' => 'History'],
        ['category' => 'Health & Fitness'],
        ['category' => 'Cooking'],
        ['category' => 'Arts & Music'],
        ['category' => 'Business'],
        ['category' => 'Comics'],
        ['category' => 'Computer & Tech'],
        ['category' => 'Biographies']
        ]);
    }
}
