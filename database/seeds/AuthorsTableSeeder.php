<?php

use Illuminate\Database\Seeder;
use App\Models\Author;

class AuthorsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Author::insert([
        ['first_name' => 'Jane', 'middle_name' => '','last_name' => 'Austen'],
        ['first_name' => 'Ahmed', 'middle_name' => 'Khaled', 'last_name' => 'Tawfeek'],
        ['first_name' => 'Markus', 'middle_name' => '','last_name' => 'Zusak'],
        ['first_name' => 'Julie', 'middle_name' => '','last_name' => 'Buxbaum'],
        ['first_name' => 'Charlotte', 'middle_name' => '', 'last_name' => 'Brontë']
        ]);
    }
}
