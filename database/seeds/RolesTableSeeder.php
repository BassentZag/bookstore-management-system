<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert([
          [
          'id' => 0,
          'name' => 'admin'
          ],
          [
            'id' => 1,
            'name' => 'senior'
          ],
          [
            'id' => 2,
            'name' => 'employee'
          ]
        ]);
    }
}
