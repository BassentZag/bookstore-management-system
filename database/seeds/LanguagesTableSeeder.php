<?php

use Illuminate\Database\Seeder;
use App\Models\Language;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    //  $languages = [['english'], ['arabic'], ['french'], ['german'], ['chinese']];
      Language::insert([
        ['language' => 'english'],
        ['language' => 'arabic'],
        ['langauge' => 'french'],
        ['language' => 'german'],
        ['language' => 'chinese']
        ]);

    }
}
