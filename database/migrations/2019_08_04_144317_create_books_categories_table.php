<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books_categories', function (Blueprint $table) {
            $table->char('book_isbn', 13);
            $table->tinyInteger('category_id');

            //specifying the primary.
            $table->primary(['book_isbn', 'category_id']);

            //specifying foreign key constraints.
            $table->foreign('book_isbn')->references('ISBN')->on('books')->onDelete('cascade');
            $table->foreign('category_id')->references('category_id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books_categories');
    }
}
