<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
          $table->tinyIncrements('employee_id');
          $table->string('first_name');
          $table->string('last_name');
          $table->string('username')->unique();
          //TODO!
          $table->string('password');
          $table->tinyInteger('email_id');

          // indexing
          $table->index('username');
          $table->unique(['first_name', 'last_name']);

          //foreign key referencing
          $table->foreign('email_id')->references('email_id')->on('emails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employees');
    }
}
