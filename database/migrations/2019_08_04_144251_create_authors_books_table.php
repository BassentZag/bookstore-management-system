<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorsBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors_books', function (Blueprint $table) {
          $table->tinyInteger('author_id');
          $table->char('book_isbn', 13);

          //specifying the primary.
          $table->primary(['author_id', 'book_isbn']);

          //specifying foreign key constraints.
          $table->foreign('author_id')->references('author_id')->on('authors');
          $table->foreign('book_isbn')->references('ISBN')->on('books')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors_books');
    }
}
