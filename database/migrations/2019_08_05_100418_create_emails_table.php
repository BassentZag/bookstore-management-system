<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('emails', function (Blueprint $table) {
            $table->tinyInteger('email_id')->autoIncrement(); //primary key
            $table->string('user_email');
            $table->tinyInteger('domain_id');

            // indexing
            $table->unique(['user_email', 'domain_id']);

            //specifying foreign key constraints.
            $table->foreign('domain_id')->references('domain_id')->on('email_domains');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('emails');
    }
}
