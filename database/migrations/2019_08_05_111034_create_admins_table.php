<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->tinyIncrements('admin_id');
            $table->string('username')->unique();
            //TODO!
            $table->string('password');
            $table->tinyInteger('email_id');

            // indexing
            $table->index('username');

            //foreign key referencing
            $table->foreign('email_id')->references('email_id')->on('emails');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
