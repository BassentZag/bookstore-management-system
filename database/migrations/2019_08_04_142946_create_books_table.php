<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
          //TODO:: isbn integer or string?
            $table->char('ISBN', 13); //ISBN 13-format
            $table->string('title')->nullable(false);  // Book Title
            $table->longText('description')->nullable();  // Book Description
            $table->char('publication_year', 4)->nullable(false); // Publication Year
            $table->unsignedBigInteger('copies_available')->nullable(false); // Number of copies available
            $table->tinyInteger('language')->nullable(false);

            // primary key
            $table->primary('ISBN');


            // indexing
            $table->index('title');
            $table->index('publication_year');
            $table->index('language');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('books');
    }
}
