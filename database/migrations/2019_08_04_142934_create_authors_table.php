<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuthorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('authors', function (Blueprint $table) {
            //Auto-incrementing UNSIGNED TINYINT (primary key) equivalent column.
            $table->tinyInteger('author_id')->autoIncrement();
            $table->string('first_name')->nullable(false);
            $table->string('middle_name');
            $table->string('last_name')->nullable(false);

            //indexing
            $table->index(['first_name', 'last_name']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('authors');
    }
}
