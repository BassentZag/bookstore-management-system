<?php

Route::group([
    'namespace' => 'API',
    'prefix' => 'users/actions',
], function () {
    // user signup : /users/actions/signup
    // admin must signup new users to the system
    Route::post('signup', 'UserController@createUser')->middleware(['auth:api', 'adminAuth']);

    // user login : /users/actions/login
    Route::post('login', 'UserController@authenticateUser');

    // after email confirmation [user activation] : /users/actions/signup/activate/{token}
    Route::get('signup/activate/{token}', 'UserController@activateUser');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        // user logout : /users/actions/logout
        Route::get('logout', 'UserController@revokeUserToken');

        Route::post('create-password', 'PasswordController@create');
        Route::post('reset-password', 'PasswordController@update');
    });
});

Route::group([
    'namespace' => 'API',
//    'middleware' => 'auth:api',
    'prefix' => 'users',
    ], function() {
        // get user
        Route::get('user', 'UserController@index')->middleware('auth:api');

        // get user by token : /users/{token}
        Route::get('{token}', 'PasswordController@read');
});

Route::group([
    'middleware' => ['auth:api', 'permission'],
    'namespace' => 'API',
  ], function () {

  // Read: a single book using its isbn.
  Route::get('/books/{isbn}', 'BookController@read');

  // Read: a collection of books.
  Route::get('/books', 'BookController@readBooks');

  // Delete a book by isbn.
  Route::delete('/books/{isbn}', 'BookController@delete');

  // store a newly created book record.
  Route::post('/books', 'BookController@create');

  // update an already-existing book record.
  Route::put('/books/{isbn}', 'BookController@update');
});
