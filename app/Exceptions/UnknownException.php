<?php


namespace App\Exceptions;


use App\Constants\UserManagement\Error;
use App\Services\ResponseService;

class UnknownException extends AppException
{

    /**
     * UnknownException constructor.
     */
    public function __construct($errorBody)
    {
        parent::__construct(/*$responseService, */"", $errorBody);
    }

    public function report()
    {
        // TODO: Implement report() method.
    }

    public function render($request)
    {
        return ResponseService::getErrorResonse(Error::USER_NOT_ADDED, $this->errorBody);
    }
}
