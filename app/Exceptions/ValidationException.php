<?php


namespace App\Exceptions;


use App\Services\Interfaces\ResponseServiceInterface;
use App\Services\ResponseService;

class ValidationException extends AppException
{

    public function ValidationException(/*$responseService, */$description, $errorBody){
        parent::__construct(/*$responseService,*/ $description, $errorBody);
    }

    public function report()
    {
        // TODO: Implement report() method.
    }

    public function render($request)
    {
        return ResponseService::getErrorResonse($this->description, $this->errorBody);
    }
}
