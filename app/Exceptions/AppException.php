<?php


namespace App\Exceptions;
use Exception;
use App\Services\Interfaces\ResponseServiceInterface;

abstract class AppException extends Exception
{

    /*protected $responseService;*/
    protected $description;
    protected $errorBody;

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getErrorBody()
    {
        return $this->errorBody;
    }

    /**
     * @param mixed $errorBody
     */
    public function setErrorBody($errorBody): void
    {
        $this->errorBody = $errorBody;
    }

    /**
     * AppException constructor.
     * @param ResponseServiceInterface $responseService
     */
    public function __construct(/*ResponseServiceInterface $responseService, */$description, $errorBody)
    {
      /*  $this->responseService = $responseService;*/
        $this->description = $description;
        $this->errorBody = $errorBody;
    }

    abstract public function report();
    abstract public function render($request);
}
