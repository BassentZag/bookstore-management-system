<?php


namespace App\Exceptions;


use App\Services\ResponseService;

class AuthenticationException extends AppException
{

    /**
     * AuthenticationException constructor.
     */
    public function __construct($description, $errorBody)
    {
        parent::__construct(/*$responseService, */$description, $errorBody);
    }

    public function report()
    {
        // TODO: Implement report() method.
    }

    public function render($request)
    {
        return ResponseService::getErrorResonse($this->description, $this->errorBody);
    }
}
