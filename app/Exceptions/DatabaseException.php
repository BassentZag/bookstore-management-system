<?php


namespace App\Exceptions;


use App\Constants\BookRetrievingMessages\Error;
use App\Services\Interfaces\ResponseServiceInterface;
use App\Services\ResponseService;

class DatabaseException extends AppException
{

    function DatabaseException(/*$responseService, */$description, $errorBody) {
        parent::__construct(/*$responseService, */$description, $errorBody);
    }
    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render($request)
    {
        return ResponseService::getErrorResonse($this->description, $this->errorBody);
    }

    public function report()
    {
        // TODO: Implement report() method.
    }
}
