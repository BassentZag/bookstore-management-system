<?php


namespace App\Repositories;


use App\Models\PasswordReset;
use App\Repositories\Interfaces\PasswordRepoInterface;
use Illuminate\Http\Request;

class PasswordRepo implements PasswordRepoInterface
{
    public function createPassword($email)
    {
        $passwordReset = PasswordReset::updateOrCreate(
          ['email' => $email],
          [
              'email' => $email,
              'token' => str_random(60)
           ]
        );
        return $passwordReset;
    }

    public function find($token)
    {
        return PasswordReset::where('token', $token)->first();
    }

    public function reset(Request $request)
    {
        // TODO: Implement reset() method.
    }
    public function deleteToken($token) {
        $token->delete();
    }

    public function authenticateUser($email, $token) {
        return PasswordReset::where([
            ['token', $token],
            ['email', $email]
        ])->first();
    }
}
