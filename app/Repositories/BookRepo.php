<?php

namespace App\Repositories;

use App\Repositories\Interfaces\BookRepoInterface;
use App\Models\Book;

class BookRepo implements BookRepoInterface
{

  public function getBooks() {
    return Book::simplePaginate(15);
  }

  public function getBookByISBN($isbn) {
    return Book::find($isbn);
  }

  public function deleteBookByISBN($isbn) {
    return Book::destroy($isbn);
  }

  public function addBook($bookISBN, $bookTitle, $bookDescription,
                            $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory) {
    // first we need to search for language, if not found then add it to the language table.
    $lang = \App\Models\Language::where('language', $bookLanguage)->first();
    if(empty($lang)) {
      $lang = new \App\Models\Language;
      $lang->language = $bookLanguage;
      $lang->save();
    }

    $authorID = array();
    foreach($bookAuthor as $bauthor) {
        $author = \App\Models\Author::where('first_name', $bauthor['first_name'])->where('last_name', $bauthor['last_name'])->first();
        if(empty($author)) {
            $author = new \App\Models\Author;
            $author->first_name = $bauthor[0];
            $author->middle_name = $bauthor[1];
            $author->last_name = $bauthor[2];
            $author->save();
        }
        $authorID [] = $author->author_id;
    }

    $categoryID = array();
    foreach($bookCategory as $bcategory) {
        $category = \App\Models\Category::where('category', $bcategory)->first();
        if(empty($category)) {
            $category = new \App\Models\Category;
            $category->category = $bcategory;
            $category->save();
        }
        $categoryID [] = $category->category_id;
    }
    //then we just add the book specified, it might fail if the same isbn is present.
    $book = new Book;
    $book->ISBN = $bookISBN;
    $book->title = $bookTitle;
    $book->description = $bookDescription;
    $book->publication_year = $bookPublicationYear;
    $book->copies_available = $copiesAvailable;
    $book->language = $lang->language_id;


    // if above lines succeed.
    // we'll add to the pivot tables
    $book->save();
    foreach($authorID as $author)
        $book->authors()->attach($author);

    foreach($categoryID as $category)
        $book->categories()->attach($category);
  }

  public function updateBookByISBN($isbn, $bookISBN, $bookTitle, $bookDescription,
                                   $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory) {
      DB::transaction(function($isbn, $bookISBN, $bookTitle, $bookDescription,
                               $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory) {
          $this->deleteBookByISBN($isbn);
          $this->addBook($bookISBN, $bookTitle, $bookDescription,
              $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory);
      });
  }
}
