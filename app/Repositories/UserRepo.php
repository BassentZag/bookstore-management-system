<?php

namespace App\Repositories;


use App\Services\Interfaces\AuthenticationServiceInterface;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\SignupActivate;
use App\Notifications\WelcomeActivate;
use App\Models\Role;
use App\Repositories\Interfaces\UserRepoInterface;



class UserRepo implements UserRepoInterface
{

    public function signup($username, $email, $role, $password) {
       $user = new User([
          'username' => $username,
          'email' => $email,
          'role' => $role,
          'password' => bcrypt($password),
          'activation_token' => str_random(60)
      ]);
      $user->save();
      return $user;
    }


    public function login($request, $is_remember_me) {
      $tokenResult = $request->user()->createToken('Password Grant Token');
      $token = $tokenResult->token;

      if ($is_remember_me)
          $token->expires_at = Carbon::now()->addWeeks(1);

      $token->save();
      return $tokenResult;
    }

    public function logout($user) {
      $user->token()->revoke();
    }



    public function activateUser($token) {
      $user = User::where('activation_token', $token)->first();
      if(empty($user))
          return;
      $user->active = true;
      $user->activation_token = '';
      $user->save();

      return $user;
    }

    public function findUserByEmail($email){
        return User::where('email', $email)->first();
    }

}
