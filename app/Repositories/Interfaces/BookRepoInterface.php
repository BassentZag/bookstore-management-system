<?php

namespace App\Repositories\Interfaces;



/**
 *
 */
interface BookRepoInterface
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getBooks();

    public function getBookByISBN($isbn);

    public function deleteBookByISBN($isbn);

    public function addBook($bookISBN, $bookTitle, $bookDescription,
                              $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory);
    public function updateBookByISBN($isbn, $bookISBN, $bookTitle, $bookDescription,
                                     $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory);
}
