<?php


namespace App\Repositories\Interfaces;


use Illuminate\Http\Request;

Interface PasswordRepoInterface
{
    public function createPassword($email);
    public function find($token);
    public function reset(Request $request);
    public function deleteToken($token);
    public function authenticateUser($email, $token);
}
