<?php

namespace App\Http\Controllers\API;

use App\Constants\UserManagement\Success;
use App\Http\Requests\PasswordPutRequest;
use App\Services\ResponseService;
use App\Http\Controllers\Controller;
use App\Http\Requests\PasswordResetRequest;
use App\Services\Interfaces\PasswordServiceInterface;
use App\PasswordReset;

class PasswordController extends Controller
{

    private $passwordResetService;

    public function __construct(PasswordServiceInterface $passwordResetService)
    {
      $this->passwordResetService = $passwordResetService;
    }
  /**
   * Create token password reset
   *
   * @param  [string] email
   * @return [string] message
   */
  public function create(PasswordResetRequest $request)
  {
      $this->passwordResetService->createPassword($request->email);
      return ResponseService::getSuccessResponse(Success::SUCCESS, Success::RESET_EMAIL);
  }
  /**
   * Find token password reset
   *
   * @param  [string] $token
   * @return [string] message
   * @return [json] passwordReset object
   */
  public function read($token)
  {
      $passwordResetToken = $this->passwordResetService->find($token);
      return ResponseService::getSuccessResponse(Success::Q_OK, $passwordResetToken);
     // return $token;
  }
  /**
   * Reset password
   *
   * @param  [string] email
   * @param  [string] password
   * @param  [string] password_confirmation
   * @param  [string] token
   * @return [string] message
   * @return [json] user object
   */
  public function update(PasswordPutRequest $request)
  {
    $user_new_cred =  $this->passwordResetService->reset($request->email, $request->token, $request->password);
      return ResponseService::getSuccessResponse(Success::SUCCESS, $user_new_cred);

  }
}
