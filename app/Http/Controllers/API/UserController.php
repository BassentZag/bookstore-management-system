<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\UserLoginRequest;
use App\Http\Requests\UserPostRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    private $authenticationController;
    private $passwordResetController;
    /**
     * UserController constructor.
     * @param AuthenticationController $authenticationController
     * @param PasswordController $passwordResetController
     */
    public function __construct(AuthenticationController $authenticationController,
                                PasswordController $passwordResetController)
    {
        $this->authenticationController = $authenticationController;
        $this->passwordResetController = $passwordResetController;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createUser(UserPostRequest $request)
    {
       return $this->authenticationController->signup($request->validated());
    }

    /**
     * Login
     */
    public function authenticateUser(UserLoginRequest $request)
    {
       return $this->authenticationController->login($request);
    }

    public function activateUser($token) {
        return $this->authenticationController->activateUser($token);
    }

    public function index(Request $request) {
        return $this->authenticationController->getUser($request);
    }

    public function revokeUserToken(Request $request) {
        return $this->authenticationController->logout($request);
    }
}
