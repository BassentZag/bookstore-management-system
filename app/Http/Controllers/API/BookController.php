<?php

namespace App\Http\Controllers\API;

use App\Constants\BookRetrievingMessages\Error;
use App\Constants\BookRetrievingMessages\Success;
use App\Http\Requests\BookPostRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookPutRequest;
use App\Services\Interfaces\BookServiceInterface;
use App\Services\ResponseService;
class BookController extends Controller
{

    private $bookService;
/*    private $responseService;*/

    public function __construct(/*ResponseServiceInterface $responseService, */BookServiceInterface $bookService)
    {
        $this->bookService = $bookService;
        /*$this->responseService = $responseService;*/
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function readBooks()
    {
        // with pagination
        $books = $this->bookService->getBooks();

        // if no books are found in the database then send a 204 response.
        if(!count($books)) {
            return ResponseService::getSuccessResponse(Success::NO_BOOKS_FOUND, "");
        }

        // books found in the database.
        return ResponseService::getSuccessResponse(Success::SUCCESS, $books);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $isbn
     * @return \Illuminate\Http\Response
     */
    public function read($isbn)
    {
        $book = $this->bookService->getBookByISBN($isbn);

        // if no such book is found return a 204 no content.
        if(empty($book)) {
            return ResponseService::getErrorResonse(Error::NO_SUCH_BOOK_FOUND, "");
        }

        // book found in the database.
        return ResponseService::getSuccessResponse(Success::SUCCESS, $book);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $isnn
     * @return \Illuminate\Http\Response
     */
    public function delete($isbn)
    {
        // if no action performed on database.
        if(!$this->bookService->deleteBookByISBN($isbn)) {
            return ResponseService::getErrorResonse(Error::NO_SUCH_BOOK_FOUND, "");
        } else {
            return ResponseService::getSuccessResponse(Success::SUCCESS, Success::SUCCESS);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function create(BookPostRequest $request)
    {
        $this->bookService->addBook($request->ISBN, $request->title, $request->description,
            $request->publication_year, $request->copies_available, $request->language, $request->authors, $request->category);
        return ResponseService::getSuccessResponse(Success::SUCCESS, Success::SUCCESS);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BookPutRequest $request, $id)
    {
        $this->bookService->updateBookbyIsbn($id, $request->ISBN, $request->title, $request->description,
            $request->publication_year, $request->copies_available, $request->language, $request->authors, $request->category);
    }
}
