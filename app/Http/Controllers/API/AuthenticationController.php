<?php

namespace App\Http\Controllers\API;

use App\Constants\UserManagement\Success;
use App\Services\ResponseService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Interfaces\AuthenticationServiceInterface;



class AuthenticationController extends Controller
{

    private $authService;


    public function __construct(AuthenticationServiceInterface $authService)
    {
      $this->authService = $authService;
    }


  /**
       * Create user
       *

       * @param  [string] name
       * @param  [string] email
       * @param  [string] password
       * @param  [string] password_confirmation
       * @return [string] message
       */
      public function signup($request)
      {
          $this->authService->signup($request['username'], $request['email'], $request['role'], $request['password']);
          return ResponseService::getSuccessResponse(Success::SUCCESS, Success::EMAIL_SENT);
      }


    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function login(Request $request)
    {
        $cred = $this->authService->login($request, $request->email, $request->password, $request->remember_me);
        return ResponseService::getSuccessResponse(Success::Q_OK, $cred);
    //    $user = $request->user('api');
     //   return ResponseService::getSuccessResponse(Success::Q_OK, $user);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {
       $this->authService->logout($request->user());
       return ResponseService::getSuccessResponse(Success::OK, "");
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function getUser(Request $request)
    {
      return ResponseService::getSuccessResponse(Success::Q_OK, $request->user());
    }

    public function activateUser($token)
    {
      $userCred = $this->authService->activateUser($token);
      return ResponseService::getSuccessResponse(Success::ACTIV_SUCCESS, $userCred);
    }
}
