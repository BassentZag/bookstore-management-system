<?php

namespace App\Http\Middleware;

use App\Constants\UserRole;
use App\Services\AuthorizationService;
use Closure;

class CheckIfAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      //  $request->user('api')->authorizeRoles(UserRole::ADMIN);
        AuthorizationService::authorizeRoles($request->user('api'), UserRole::ADMIN);
        return $next($request);
    }
}
