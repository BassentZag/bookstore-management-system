<?php

namespace App\Http\Middleware;

use App\Constants\UserRole;
use App\Services\AuthorizationService;
use Closure;

class CheckUserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if ($request->isMethod('delete')) {
          AuthorizationService::authorizeRoles($request->user('api'), UserRole::ADMIN);
      } else if ($request->isMethod('post') || $request->isMethod('put')) {
          AuthorizationService::authorizeRoles($request->user('api'), [UserRole::ADMIN, UserRole::SENIOR]);
      } else {
          AuthorizationService::authorizeRoles($request->user('api'), [UserRole::ADMIN, UserRole::SENIOR, UserRole::EMP]);
      }
       return $next($request);
    }
}
