<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Constants\BookRetrievingMessages\Error;
use App\Exceptions\ValidationException;
use App\Services\ResponseService;
use Illuminate\Contracts\Validation\Validator;


class UserPostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|unique:users|alpha_num',
            'email' => 'required|unique:users|email',
            'password' => 'required',
            'role' => 'required|integer|min:0|max:2',
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new ValidationException(/*new ResponseService(), */Error::VALIDATION_ERROR, $validator->errors());
    }
}
