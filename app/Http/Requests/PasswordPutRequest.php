<?php

namespace App\Http\Requests;

use App\Constants\BookRetrievingMessages\Error;
use App\Exceptions\ValidationException;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class PasswordPutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email',
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new ValidationException(/*new ResponseService(), */Error::VALIDATION_ERROR, $validator->errors());
    }
}
