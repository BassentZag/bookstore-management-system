<?php

namespace App\Http\Requests;

use App\Constants\BookRetrievingMessages\Error;
use App\Exceptions\ValidationException;
use App\Services\ResponseService;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class BookPostRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ISBN' => 'required|unique:books|digits:13',
            'title' => 'required|string',
            'publication_year' => 'required|digits:4|digits:4',
            'copies_available' => 'required|digits_between:1,4',
            'authors.*.first_name' => 'required|Alpha',
            'authors.*.second_name' => 'Alpha|nullable',
            'authors.*.last_name' => 'required|Alpha',
            'language' => 'required|Alpha',
            'category.*' => 'required|Alpha'
        ];
    }


    protected function failedValidation(Validator $validator) {
        throw new ValidationException(/*new ResponseService(), */Error::VALIDATION_ERROR, $validator->errors());
    }

}
