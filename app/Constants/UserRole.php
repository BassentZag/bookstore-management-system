<?php


namespace App\Constants;


class UserRole
{
    const ADMIN = 'admin';
    const SENIOR = 'senior';
    const EMP = 'employee';
}
