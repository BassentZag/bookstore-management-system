<?php


namespace App\Constants;


class SuccessHttpCodeMapper
{
    // mapping of success message to http response code

    const MESSAGE_RESPONSE_CODE = [
        'No Books Found in the Database!' => HttpResponseCode::HTTP_OK, // TODO::
        'Query Performed Successfully' => HttpResponseCode::HTTP_OK,
        'Successfully created user.' => HttpResponseCode::HTTP_CREATED,
        'User Activation Success.' => HttpResponseCode::HTTP_OK,
        'Success' => HttpResponseCode::HTTP_OK,
        'Operation Performed Successfully' => HttpResponseCode::HTTP_NO_CONTENT,
    ];
}
