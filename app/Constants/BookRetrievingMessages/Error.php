<?php


namespace App\Constants\BookRetrievingMessages;


use App\Constants\HttpResponseCode;

class Error
{
    const NO_SUCH_BOOK_FOUND = 'No Such Book Found in the Database!';
    const INTEGRITY_VIOLATION = 'Integrity Constraint Violated: Cannot delete such entry.';
    const NO_CHANGE_DB = 'Zero Rows Affected.';
    const VALIDATION_ERROR = 'Validation Error.';
}
