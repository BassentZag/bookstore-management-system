<?php
namespace App\Constants\BookRetrievingMessages;





use App\Constants\HttpResponseCode;

class Success {
    const NO_BOOKS_FOUND = 'No Books Found in the Database!';
    const SUCCESS = 'Query Performed Successfully';
}
