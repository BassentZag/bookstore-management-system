<?php


namespace App\Constants\UserManagement;


use App\Constants\HttpResponseCode;

class Success
{
    const SUCCESS = 'Successfully created user.';
    const EMAIL_SENT = 'E-mail Sent to User.';
    const ACTIV_SUCCESS = 'User Activation Success.';
    const Q_OK = 'Success';
    const OK = 'Operation Performed Successfully';
    const RESET_EMAIL = 'We have e-mailed your password reset link!';

}
