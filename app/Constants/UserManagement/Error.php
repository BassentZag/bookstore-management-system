<?php


namespace App\Constants\UserManagement;


use App\Constants\HttpResponseCode;

class Error
{
    const USER_NOT_ADDED = 'Query not Performed!';
    const INVALID_TOKEN = 'Invalid Activation Token.';
    const UNAUTHORIZED = 'Unauthorized';
    const UNAUTHENTICATED = 'Unauthenticated User!';
    const EMAIL_NOT_FOUND = 'We cannot find a user with that e-mail address.';
    const INVALID_PR_TOKEN = 'This password reset token is invalid.';
}
