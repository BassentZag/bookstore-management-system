<?php


namespace App\Constants;


class ErrorHttpCodeMapper
{
    // mapping of error message to http response code

    const MESSAGE_RESPONSE_CODE = [
        'No Such Book Found in the Database!' => HttpResponseCode::HTTP_NO_CONTENT,
        'Integrity Constraint Violated: Cannot delete such entry.' => HttpResponseCode::HTTP_FORBIDDEN,
        'Validation Error.' => HttpResponseCode::HTTP_UNPROCESSABLE_ENTITY,
        'Query not Performed!' => HttpResponseCode::HTTP_INTERNAL_SERVER_ERROR,
        'Caught Exception from Database! Unsuccessful Operation.' => HttpResponseCode::HTTP_INTERNAL_SERVER_ERROR,
        'Invalid Activation Token.' => HttpResponseCode::HTTP_UNPROCESSABLE_ENTITY,
        'Unauthorized' => HttpResponseCode::HTTP_UNAUTHORIZED,
        'Unauthenticated User!' => HttpResponseCode::HTTP_FORBIDDEN,
        'We cannot find a user with that e-mail address.' => HttpResponseCode::HTTP_NOT_FOUND,
        'This password reset token is invalid.' =>HttpResponseCode::HTTP_NOT_FOUND,
    ];
}
