<?php

namespace App\Services;

use App\Constants\UserManagement\Error;
use App\Exceptions\AuthenticationException;
use App\Exceptions\DatabaseException;
use App\Repositories\Interfaces\PasswordRepoInterface;
use App\Services\Interfaces\AuthenticationServiceInterface;
use App\Services\Interfaces\PasswordServiceInterface;
use App\User;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\Models\PasswordReset;



class  PasswordService implements PasswordServiceInterface
{
    private $authenticationService;
    private $passwordRepo;
    /**
     * PasswordService constructor.
     */
    public function __construct(PasswordRepoInterface $passwordRepo, AuthenticationServiceInterface $authenticationService)
    {
        $this->authenticationService = $authenticationService;
        $this->passwordRepo = $passwordRepo;
    }

    /**
   * Create token password reset
   *
   * @param  [string] email
   * @return [string] message
   */
  public function createPassword($email)
  {
      $user = $this->authenticationService->findUserByEmail($email);
      if (empty($user))
          throw new AuthenticationException(Error::EMAIL_NOT_FOUND, Error::USER_NOT_ADDED);
      $passwordReset = $this->passwordRepo->createPassword($email);
      if (!$user || !$passwordReset) {
          throw new DatabaseException(Error::USER_NOT_ADDED, Error::USER_NOT_ADDED);
      }
      $user->notify(new PasswordResetRequest($passwordReset->token));
  }
  /**
   * Find token password reset
   *
   * @param  [string] $token
   * @return [string] message
   * @return [json] passwordReset object
   */
  public function find($token)
  {
      $passwordReset = $this->passwordRepo->find($token);
      if (empty($passwordReset))
          throw new AuthenticationException(Error::INVALID_PR_TOKEN, Error::USER_NOT_ADDED);
      if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
          $this->passwordRepo->deleteToken($passwordReset);
          throw new AuthenticationException(Error::INVALID_PR_TOKEN, Error::USER_NOT_ADDED);
      }
      return $token;
  }

  /**
   * Reset password
   *
   * @param  [string] email
   * @param  [string] password
   * @param  [string] password_confirmation
   * @param  [string] token
   * @return [string] message
   * @return [json] user object
   */
  public function reset($email, $token, $password)
  {

   $passwordReset = $this->passwordRepo->authenticateUser($email, $token);
   if (!$passwordReset) {
       throw new AuthenticationException(Error::INVALID_PR_TOKEN, Error::USER_NOT_ADDED);
   }
   $user = $this->authenticationService->findUserByEmail($email);
   if (!$user) {
       throw new AuthenticationException(Error::EMAIL_NOT_FOUND, Error::USER_NOT_ADDED);
   }

 //  $this->authenticationService->resetUserPassword($user, $password);
      $user->password = bcrypt($password);
      $user->save();
      $passwordReset->delete();
      $user->notify(new PasswordResetSuccess($passwordReset));
      return $user;
  }
}
