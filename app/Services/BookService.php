<?php

namespace App\Services;

use App\Constants\BookRetrievingMessages\Error;
use App\Exceptions\DatabaseException;
use App\Services\Interfaces\BookServiceInterface;
use App\Repositories\Interfaces\BookRepoInterface;

//validating
use App\Services\Interfaces\ResponseServiceInterface;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Validator;

class BookService implements BookServiceInterface
{
    private $bookRepo;
    private $responseService;

    public function __construct(BookRepoInterface $bookRepo, ResponseServiceInterface $responseService)
    {
      $this->bookRepo = $bookRepo;
      $this->responseService = $responseService;
    }


    public function getBooks(){
        return $this->bookRepo->getBooks();
    }

    public function getBookByISBN($isbn) {
        return $this->bookRepo->getBookByISBN($isbn);
    }


    public function deleteBookByISBN($isbn) {
        // we need to detect if an error occurs in the database i.e: integrity constraint violated.
        try {
            $book = $this->bookRepo->deleteBookByISBN($isbn);
        } catch (QueryException $queryException) {
            // we'll throw our custom database exception.
            throw new DatabaseException(/*$this->responseService, */Error::INTEGRITY_VIOLATION, $queryException->getMessage());
        }
        return $book;
    }


    public function addBook($bookISBN, $bookTitle, $bookDescription,
                              $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor,$bookCategory) {
        try {
            $this->bookRepo->addBook($bookISBN, $bookTitle, $bookDescription,
                $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory);
        } catch (Exception $e) {
            throw new DatabaseException(Error::NO_CHANGE_DB, $e->getMessage());
        }
    }

    public function updateBookByISBN($isbn, $bookISBN, $bookTitle, $bookDescription,
                                     $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor,$bookCategory) {
//        $this->deleteBookByISBN($bookISBN);
//        $this->addBook($bookISBN, $bookTitle, $bookDescription,
//            $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor,$bookCategory);
        try {
            $this->updateBookByISBN($isbn, $bookISBN, $bookTitle, $bookDescription,
                $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory);
        } catch (Exception $e) {
            throw new DatabaseException(Error::NO_CHANGE_DB, $e->getMessage());
        }

    }
}
