<?php


namespace App\Services;

use App\Constants\SuccessHttpCodeMapper;
use App\Constants\ErrorHttpCodeMapper;
use App\Services\Interfaces\ResponseServiceInterface;

class ResponseService implements ResponseServiceInterface
{

    public static function getSuccessResponse($successMessage, $body)
    {
        $response = [
            'Status' => [
                'Type' => 'SUCCESS',
                'Message' => $successMessage,
            ],
            'Body' => $body
        ];

        return response()->json(
            $response, SuccessHttpCodeMapper::MESSAGE_RESPONSE_CODE[$successMessage]
        );
    }

    public static function getErrorResonse($errorMessage, $body)
    {
        $response = [
            'Status' => [
                'Type' => 'ERROR',
                'Message' => $errorMessage,
            ],
            'Body' => $body
        ];

        return response()->json(
            $response, ErrorHttpCodeMapper::MESSAGE_RESPONSE_CODE[$errorMessage]
        );
    }
}
