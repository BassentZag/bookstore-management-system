<?php

namespace App\Services;

use App\Constants\DatabaseAccess;
use App\Constants\UserManagement\Error;
use App\Constants\UserManagement\Success;
use App\Constants\UsersOperations\UsersManagement;
use App\Exceptions\AuthenticationException;
use App\Exceptions\DatabaseException;
use App\Services\Interfaces\AuthenticationServiceInterface;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Notifications\SignupActivate;
use App\Notifications\WelcomeActivate;
use App\Repositories\Interfaces\UserRepoInterface;



class AuthenticationService implements AuthenticationServiceInterface
{
    private $userRepo;

    public function __construct(UserRepoInterface $userRepo)
    {
      $this->userRepo = $userRepo;
    }

    public function signup($username, $email, $role, $password) {
        try {
            $user = $this->userRepo->signup($username, $email, $role, $password);
        } catch (\Exception $e) {
            throw new DatabaseException(DatabaseAccess::UNKNOWN_ERR, $e->getMessage());
        }

        if($user === NULL) {
            throw new DatabaseException(UsersManagement::USER_NOT_ADDED, "");
        }

        $user->notify(new SignupActivate($user));
    }


    public function login($request, $email, $password, $is_remember_me) {
      $credentials['email'] = $email;
      $credentials['password'] = $password;
      $credentials['active'] = 1;
      $credentials['deleted_at'] = null;
      if(!Auth::attempt($credentials))
          throw new AuthenticationException(Error::UNAUTHENTICATED, "");
      $tokenResult = $this->userRepo->login($request, $is_remember_me);
      return [
          'access_token' => $tokenResult->accessToken,
          'token_type' => 'Bearer',
          'expires_at' => Carbon::parse(
              $tokenResult->token->expires_at
          )->toDateTimeString()
      ];
    }

    public function logout($user/*Request $request*/) {
      $this->userRepo->logout($user);
    }

    public function activateUser($token) {
      $user = $this->userRepo->activateUser($token);
      if (empty($user)) {
          throw new AuthenticationException(Error::INVALID_TOKEN, "");
      }
      $user->notify(new WelcomeActivate($user));
      return $user;
    }

    public function findUserByEmail($email) {
       return $this->userRepo->findUserByEmail($email);
    }

}
