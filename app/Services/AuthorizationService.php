<?php


namespace App\Services;


use App\Constants\UserManagement\Error;
use App\Exceptions\AuthorizationException;
use App\Models\Role;

class AuthorizationService
{

    public static function authorizeRoles($user, $roles){
        if ((is_array($roles) && !self::hasAnyRole($user, $roles)) || (!self::hasRole($user, $roles) )) {
           throw new AuthorizationException(Error::UNAUTHORIZED, "");
        }
        return true;
    }

    private static function hasRole($user, $role){
        return $user->role === Role::where('name', $role)->first()->id;
    }

    private static function hasAnyRole($user, $roles){
        $userRole = Role::select('name')->where('id', $user->role)->get();
        return NULL !== array_search((string) $userRole, $roles);
    }

}
