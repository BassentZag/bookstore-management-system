<?php

namespace App\Services\Interfaces;
use Illuminate\Http\Request;


/**
 *
 */
interface PasswordServiceInterface
{
      public function createPassword($email);
      public function find($token);
      public function reset($email, $token, $password);

}
