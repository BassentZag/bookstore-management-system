<?php

namespace App\Services\Interfaces;



/**
 *
 */
interface AuthenticationServiceInterface
{
  public function signup($username, $email, $role, $password);
  public function login($request, $email, $password, $is_remember_me);
  public function logout($user);
//  public function getUser(Request $request);
  public function activateUser($token);

  public function findUserByEmail($email);

}
