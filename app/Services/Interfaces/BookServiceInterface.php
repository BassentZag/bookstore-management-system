<?php

namespace App\Services\Interfaces;



/**
 *
 */
interface BookServiceInterface
{

  public function getBooks();

  public function getBookByISBN($isbn);

  public function deleteBookByISBN($isbn);

  public function addBook($bookISBN, $bookTitle, $bookDescription,
                            $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory);
  public function updateBookByISBN($isbn, $bookISBN, $bookTitle, $bookDescription,
                                     $bookPublicationYear, $copiesAvailable, $bookLanguage, $bookAuthor, $bookCategory);
}
