<?php


namespace App\Services\Interfaces;


interface ResponseServiceInterface
{
    public static function getSuccessResponse($successMessage, $body);
    public static function getErrorResonse($errorMessage, $body);
}
