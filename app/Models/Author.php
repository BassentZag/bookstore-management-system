<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Author extends Model {
    protected $table = 'authors';
    protected $primaryKey = 'author_id';
    public $timestamps = false;
    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    protected $fillable = array('first_name', 'middle_name', 'last_name');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each Book HAS one language to eat
    public function books() {
        return $this->belongsToMany('\App\Models\Book', 'authors_books', 'author_id', 'book_id');
    }
}
