<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Language extends Model {

    protected $table = 'languages';
    protected $primaryKey = 'language_id';
    public $timestamps = false;


    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    protected $fillable = array('language');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each Book HAS one language to eat
    public function books() {
        return $this->belongsTo('\App\Models\Book'); // this matches the Eloquent model
    }
}
