<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model {

    protected $table = 'books';
    protected $primaryKey = 'ISBN';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;



    protected $fillable = array('ISBN', 'title', 'description', 'publication_year', 'copies_available','language');




    public function languages() {
        return $this->hasOne('Language'); // this matches the Eloquent model
    }

    public function categories() {
        return $this->belongsToMany('\App\Models\Category', 'books_categories');
    }

    public function authors() {
        return $this->belongsToMany('\App\Models\Author', 'authors_books', 'book_isbn', 'author_id');
    }

}
