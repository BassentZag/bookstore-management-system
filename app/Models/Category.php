<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
class Category extends Model {
    public $timestamps = false;

    // MASS ASSIGNMENT -------------------------------------------------------
    // define which attributes are mass assignable (for security)
    protected $fillable = array('category');

    // DEFINE RELATIONSHIPS --------------------------------------------------
    // each Book HAS one language to eat
    public function books() {
        return $this->belongsToMany('\App\Models\Book', 'books_categories', 'book_isbn', 'category_id'); // this matches the Eloquent model
    }
}
