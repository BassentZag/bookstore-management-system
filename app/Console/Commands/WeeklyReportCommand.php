<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Storage;

class WeeklyReportCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'week:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Report books out of stock weekly.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      Storage::append('schedulerLog.txt','scheduled task performed!');
    }
}
