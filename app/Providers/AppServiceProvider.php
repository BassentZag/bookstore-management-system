<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    //    $this->app->bind('App\Services\Interfaces\MailServiceInterface', 'App\Services\MailService');
        $this->app->bind('App\Services\Interfaces\ResponseServiceInterface',
                        'App\Services\ResponseService');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
