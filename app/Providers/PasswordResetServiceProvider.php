<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class PasswordResetServiceProvider extends ServiceProvider
{


    public function register() {
      $this->app->bind('App\Services\Interfaces\PasswordServiceInterface', 'App\Services\PasswordService');
      $this->app->bind('App\Repositories\Interfaces\PasswordRepoInterface', 'App\Repositories\PasswordRepo');
    }

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
      //
    }
}
